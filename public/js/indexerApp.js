'use strict';

var app = angular.module("indexerApp",['ui.bootstrap','indexerMisc'],null); //TODO: wtf null...

app.controller("mainCtrl",function($scope,$http,$window){
    $scope.url = 'http://www.example.com';
    $scope.searchText = 'Land Cruiser';
    $scope.action = 'checkStatus';
    $scope.message = '<h5>Enter a target url and select action</h5>';
    $scope.links = [];
    $scope.displayAdd = false;
    $scope.displayDomain = false;
    $scope.stat ={'query':0,'progress':0,'done':0};

    $scope.getActionName=function(action){
        if(!action){
            action = $scope.action;
        }
        switch (action){
            case 'toQuery':
                return 'Add to indexing query';
                break;
            case 'checkStatus':
                return 'Check page status';
                break;
            case 'domainInfo':
                return 'Check domain info';
                break;
            default :
                return 'Unknown action';
        }
    }
    $scope.sendRequest = function(opt_act){
        var action = 'check';
        if(opt_act){
            $scope.action = opt_act;
        }
        switch ($scope.action){
            case 'toQuery':
                action = 'toquery';
                break;
            case 'checkStatus':
                action = 'check';
                break;
            case 'getStat':
                action = 'stat';
                break;
            case 'domainInfo':
                action = 'domaininfo';
                break;
        }
        $http({method:"POST",url:action,data:{url:$scope.url}})
            .success(function(data, status, headers, config) {
                switch ($scope.action){
                    case 'toQuery':
                        if(data){
                            $scope.message += '<p><h5>Page:'+$scope.url+' successfuly added to query</h5>';
                            $scope.displayAdd = false;
                        }else{
                            $scope.message += '<p><h5>Page:'+$scope.url+' already in database.</h5>';
                            $scope.displayAdd = false;
                        }
                        break;
                    case 'checkStatus':
                        $scope.message =$scope.checkTemplate(data);
                        $scope.displayDomain = true;
                        break;
                    case 'getStat':
                        $scope.stat ={'query':data.query,'progress':data.progress,'done':data.done};
                        break;
                    case 'domainInfo':
                        $scope.message =$scope.domainTemplate(data);
                        break;
                }
            }).error(function(data, status, headers, config) {
                $scope.message = 'Response failed! Status:'+status;
            });
    }

    $scope.sendRequest('getStat');

    $scope.sendSearchRequest = function(){
        $http({method:"POST",url:'search',data:{text:$scope.searchText}})
            .success(function(data, status, headers, config) {
                if(data.length > 1){
                    $scope.links = data;
                    $scope.searchResult = '';
                }else{
                    $scope.searchResult = 'Sorry...No matches';
                    $scope.links = [];
                }
            }).error(function(data, status, headers, config) {
                $scope.message = status;
            });
    }
    $scope.checkTemplate = function(data){
        var status;
        switch (data.status){
            case '1':
                status = 'Await indexing';
                break;
            case '2':
                status = 'Indexing in process';
                break;
            case '3':
                status = 'Ready';
                break;
            default:
                status = 'Not in database';
        }
        var result ='';
        if(status == 'Not in database'){
            result += '<h5>Page status: '+status+'</h5><p>';
            result += '<h5>You can add this page to indexing query</h5>'
            $scope.displayAdd = true;
        }else{
            if(status=='Ready'){
                result += '<label class="well well-small">'+' <a  href = "'+$scope.url+'">'+data.title+'</a>'+' <a class="label label-info pull-right" href = "/preview?url='+data.hash_url+'">' +
                    '<small> Saved copy</small></a> </label> ';
            }
            result +='<p>Domain: '+data.domain+' </p>';
            result +='<p>indexed: '+data.date+' </p>';
            result += '<h5>Page status: '+status+'</h5><p>';
        }
        return result;
    }

    $scope.domainTemplate = function(data){
        var status;
        var result ='';
        result += '<h4>'+data.domain+' stat:</h4><p>';
        result += '<h5>In query: '+data.query+'</h5><p>';
        result += '<h5>In progress: '+data.progress+'</h5><p>';
        result += '<h5>Done: '+data.done+'</h5><p>';
        if(data.pages.length >1){
            result += '<a>Show page list</a></p>';
        }
        for(var key in data.pages){
            result += '<h6>'+data.pages[key].url+' - '+$scope.statusMsg(data.pages[key].status)+'</h6><p>';
        }
        return result;
    }
    $scope.statusMsg = function(num){
        switch (num){
            case '1':
                return 'Await indexing';
            case '2':
                return 'Indexing in process';
            case '3':
                return 'Ready';
            default:
                return 'Not in database';
        }
    }

});

