<?php
/**
 * Created by JetBrains PhpStorm.
 * User: istrelnikov
 * Date: 9/19/13
 * Time: 4:43 PM
 * To change this template use File | Settings | File Templates.
 */

namespace Application\Model;

use Zend\Dom\Query;

class Crawler {
    public static function getPage($link){
        file_put_contents('debug.txt',$link."\r\n",FILE_APPEND);
        require_once('module/Application/src/Application/Misc/url_to_absolute.php');
        $page = array();
        if(Crawler::retrieve_remote_file_size($link)>1024*1024){ // big files
            return null;
        }
        $page['html'] = Crawler::get_url_data($link);
        if(!$page['html']){
            return null;
        }
        /* Get the MIME type and character set */
        preg_match( '@<meta\s+http-equiv="Content-Type"\s+content="([\w/]+)(;\s+charset=([^\s"]+))?@i',
            $page['html'], $matches );
        if (isset($matches[3]))
            $initialEncoding = $matches[3];

        if(isset($initialEncoding)){
            if( $initialEncoding != 'UTF-8' ){
                $page['html'] = preg_replace('/<meta(.+)charset(.+)>/i','<meta http-equiv="content-type" content="text/html; charset=utf-8">', $page['html']);
                $page['html'] = mb_convert_encoding($page['html'],'UTF-8',$initialEncoding);
                }
        }else{
            //TODO: good search for html5 charset ->curl charset -> default 'latin1'
            $charset = mb_detect_encoding($page['html']);
            if($charset!='UTF-8'){
                $page['html'] = mb_convert_encoding($page['html'],'UTF-8',$charset);
            }
            $page['html'] = '<meta http-equiv="content-type" content="text/html; charset=utf-8">' . $page['html'];
        }
        file_put_contents('debug.txt',"preDom\r\n",FILE_APPEND);
       /*** a new dom object ***/
        $dom = new Query($page['html']);
        file_put_contents('debug.txt',"afterDom\r\n",FILE_APPEND);

        /*** plaintext to index ***/
        $page['plain'] = '';
        file_put_contents('debug.txt',"preTextQuery\r\n",FILE_APPEND);
        $text_tags = $dom->queryXpath("//text()[not(ancestor::script)][not(ancestor::style)][not(ancestor::noscript)][not(ancestor::form)]");
        file_put_contents('debug.txt',"afterTextQuery: count:".count($text_tags)."\r\n",FILE_APPEND);
        foreach( $text_tags as $text){
           if(strlen(trim($text->nodeValue)) > 3){
                $page['plain'] .= trim($text->nodeValue) . ' ';
            }
        };
        file_put_contents('debug.txt',"afterPlain\r\n",FILE_APPEND);
        /*** title ***/
        $title_tag = $dom->queryXpath('//title');
        foreach($title_tag as $tag){
            $page['title'] = $tag->childNodes->item(0)->nodeValue;
        }
        file_put_contents('debug.txt',"afterTitle\r\n",FILE_APPEND);
        $a_tags = $dom->queryXpath('//a');
        /*** links ***/
        $links = array();
        foreach ($a_tags as $tag){
            $uri = Crawler::prepareUri($tag->getAttribute('href'));
            $uri = url_to_absolute($link,$uri);
            $links[] = $uri;
        }
        file_put_contents('debug.txt',"afterLinks\r\n",FILE_APPEND);
        $frame_tags = $dom->queryXpath('//iframe');
        foreach ($frame_tags as $tag){
            $uri = Crawler::prepareUri($tag->getAttribute('src'));
            $uri = url_to_absolute($link,$uri);
            $links[] = $uri;
        }
        file_put_contents('debug.txt',"afterFrames\r\n",FILE_APPEND);
        $page['links'] = $links;
        return $page;
    }

    private static function retrieve_remote_file_size($url){
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, TRUE);
        curl_setopt($ch, CURLOPT_NOBODY, TRUE);
        curl_exec($ch);
        $size = curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
        curl_close($ch);
        return $size;
    }

    private static function get_url_data($url){
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_ENCODING, '');
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.76 Safari/537.36');
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }
    public static function prepareUri($url){
        $uri = str_replace(array('\\"','\\\'','\'','"'),'',$url);
        if(substr($uri, 0, 2) === '//'){
            $uri = 'http:'.$uri;
        }
        return $uri;
    }
}