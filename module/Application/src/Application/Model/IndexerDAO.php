<?php

namespace Application\Model;
use \PDO;
use \PDOException;
use Application\Entity\SitePageEntity;

class IndexerDAO {
//params
private $host = 'localhost';
private $dbname = 'site-indexer';
private $user = 'root';
private $pass = 'root';
private $sqlchar = 'utf8';
//------
const ERROR_DUPLICATE = 1000;
const ERROR_NONE = 200;
private $DBH;

    public function __construct(){
        try {
            # MySQL через PDO_MYSQL
            $this->DBH = new PDO("mysql:host=$this->host;dbname=$this->dbname", $this->user, $this->pass,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
            $this->DBH->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
            $this->DBH->query ( 'SET character_set_connection = '.$this->sqlchar );
            $this->DBH->query ( 'SET character_set_client = '.$this->sqlchar );
            $this->DBH->query ( 'SET character_set_results = '.$this->sqlchar );
        }
        catch(PDOException $e) {
            file_put_contents('PDOErrors.txt', $e->getMessage(), FILE_APPEND);
        }
    }
    public function forceSchemaReinstall(){
        $this->DBH->exec("
            DROP TABLE IF EXISTS `pages`;
            CREATE  TABLE `pages` (
              `hash_url` VARCHAR(100) NOT NULL ,
              `domain` VARCHAR(100) NOT NULL ,
              `title` VARCHAR(100) NOT NULL ,
              `url` VARCHAR(600) NOT NULL ,
              `date` DATETIME NOT NULL ,
              `status` TINYINT NOT NULL ,
              `html` TEXT NULL ,
              PRIMARY KEY (`hash_url`) ,
              UNIQUE INDEX `hash_url_UNIQUE` (`hash_url` ASC) ,
              INDEX `domain_index` (`domain` ASC) ,
              INDEX `hash_url_index` (`hash_url` ASC) ,
              INDEX `status_index` (`status` ASC) ,
              INDEX `date_status_index` (`status` ASC, `date` ASC) ,
              INDEX `domain_status_index` USING BTREE (`domain` ASC, `status` ASC) )
            ENGINE = InnoDB
            DEFAULT CHARACTER SET = utf8;
        ");
        $this->DBH->exec('
        DROP TABLE IF EXISTS `search_index`;
        CREATE  TABLE `search_index` (
          `content` TEXT NULL ,
          `hash_url` VARCHAR(100) NOT NULL ,
          PRIMARY KEY (`hash_url`) ,
          UNIQUE INDEX `hash_url_UNIQUE` (`hash_url` ASC),
          FULLTEXT (`content`))
        ENGINE = MyISAM
        DEFAULT CHARACTER SET = utf8;
        ');
        $this->DBH->exec('
        DROP TABLE IF EXISTS `service_info`;
        CREATE  TABLE `service_info` (
          `process_id` INT NOT NULL AUTO_INCREMENT ,
          PRIMARY KEY (`process_id`) )
        ENGINE = InnoDB
        DEFAULT CHARACTER SET = utf8;
        ');
    }

    /**
     * @return SitePageEntity
     */
    public function getQueryPage(){
        try{
            $this->DBH->beginTransaction();
                $STH = $this->DBH->prepare("
                SELECT * FROM `pages` WHERE `status` = :st
                ");
                $STH->bindParam(':st',intval(IndexerModel::STATUS_AWAITING));
                $STH->execute();
                $STH->setFetchMode(PDO::FETCH_ASSOC);
                $assoc_array = $STH->fetch();
                if($assoc_array){
                    $page = new SitePageEntity($assoc_array);
                }else{
                    $this->DBH->rollBack();
                    return null;
                }
                $this->setPageStatus($page->getHashUrl(),IndexerModel::STATUS_INDEXING);
            $this->DBH->commit();
            return $page;
        }catch (PDOException $e){
            $this->DBH->rollBack();
            throw new PDOException($e->getMessage());
        }
    }
      /**
     * @param $url string
     * @return bool|string
     * @throws PDOException
     */
    public function insertNewPage($url){
        try{
            $STH = $this->DBH->prepare("
            INSERT INTO `pages` (`hash_url`, `domain`,`title`, `url`, `date`, `status`,`html`)
                    VALUES (:hash_url , :domain,:title, :url, :date, :status,:html)
            ");
            $parsed_url = parse_url($url);
            $STH->bindParam(':hash_url',md5($url));
            $STH->bindParam(':url',$url);
            $STH->bindParam(':domain',$parsed_url['host']);
            $title = 'unavailable';
            $STH->bindParam(':title',$title);
            $STH->bindParam(':date', date("Y-m-d H:i:s"));
            $STH->bindParam(':status',intval(IndexerModel::STATUS_AWAITING));
            $html = '<html></html>';
            $STH->bindParam(':html',$html);
            $STH->execute();
            return true;
        }catch (PDOException $e){
            if($e->getCode() == 23000){
                return false;
            }else{
                file_put_contents('PDOErrors.txt', $e->getMessage(), FILE_APPEND);
                throw new PDOException('Uncatched in PageDao:' .$e->getMessage());
            }
        }
    }

    public function setPagePlainText($hash,$content){
        try{
            $STH = $this->DBH->prepare("
            INSERT INTO `search_index` (`hash_url`, `content`) VALUES (:hash_url , :content)
            ");
            $STH->bindParam(':hash_url',$hash);
            $STH->bindParam(':content',$content);
            $STH->execute();
        }catch (PDOException $e){
            if($e->getCode() == 23000){
                $STH = $this->DBH->prepare("
                UPDATE `search_index` SET `content` = :content WHERE `hash_url` = :hash_url
                ");
                $STH->bindParam(':hash_url',$hash);
                $STH->bindParam(':content',$content);
                $STH->execute();
            }else{
                file_put_contents('PDOErrors.txt', $e->getMessage(), FILE_APPEND);
                throw new PDOException('Uncatched in PageDao:' .$e->getMessage());
            }
        }
    }

    public function setPageStatus($hash,$status){
        $STH = $this->DBH->prepare("
          UPDATE `pages` SET `status` = :status WHERE `hash_url` = :hash_url
        ");
        $STH->bindParam(':hash_url',$hash);
        $STH->bindParam(':status',$status);
        $STH->execute();
    }
    public function setPageTitle($hash,$title){
        $STH = $this->DBH->prepare("
        UPDATE `pages` SET `title` = :title WHERE `hash_url` = :hash_url
        ");
        $STH->bindParam(':hash_url',$hash);
        $STH->bindParam(':title',$title);
        $STH->execute();
    }
    public function setPageContent($hash,$html){
        $STH = $this->DBH->prepare("
          UPDATE `pages` SET `html` = :html WHERE `hash_url` = :hash_url
        ");
        $STH->bindParam(':hash_url',$hash);
        $STH->bindParam(':html',$html);
        $STH->execute();
    }

    public function deletePage($hash){
        $STH =  $this->DBH->prepare("DELETE FROM `pages` WHERE `hash_url` = :hash_url ");
        $STH->bindParam(':hash_url',$hash);
        $STH->execute();
    }
    public function getPageByHashUrl($hash){
        try{
            $STH = $this->DBH->prepare("SELECT * FROM `pages` WHERE `hash_url` = :hash_url");
            $STH->bindParam(':hash_url',$hash);
            $STH->execute();
            $STH->setFetchMode(PDO::FETCH_ASSOC);
            $assoc_array = $STH->fetch();
            if($assoc_array){
                return new SitePageEntity($assoc_array);
            }else{
                return null;
            }
        }catch (PDOException $e){
            file_put_contents('PDOErrors.txt', $e->getMessage(), FILE_APPEND);
            throw new PDOException('Uncatched in PageDao:' .$e->getMessage());
        }
    }
    public function getDomainInfo($domain){
        try{
            $result = array();
            $STH = $this->DBH->prepare("SELECT * FROM `pages` WHERE `domain` = :domain");
            $STH->bindParam(':domain',$domain);
            $STH->execute();
            $STH->setFetchMode(PDO::FETCH_ASSOC);
            $pages = $STH->fetchAll();
            if($pages){
                $STH = $this->DBH->prepare("SELECT count(*) FROM `pages` WHERE `domain` = :domain AND `status` = :stat");
                $STH->bindParam(':domain',$domain);
                $STH->bindParam(':stat',$stat);
                $stat = 1;
                $STH->execute();
                $result['query'] = $STH->fetchColumn();
                $stat = 2;
                $STH->execute();
                $result['progress'] = $STH->fetchColumn();
                $stat = 3;
                $STH->execute();
                $result['done'] = $STH->fetchColumn();
                $result['pages'] = $pages;
                $result['domain'] = $domain;
                return $result;
            }else{
                return null;
            }
        }catch (PDOException $e){
            file_put_contents('PDOErrors.txt', $e->getMessage(), FILE_APPEND);
            throw new PDOException('Uncatched in PageDao:' .$e->getMessage());
        }
    }

    public function getProcessCount(){
        $STH  = $this->DBH->prepare("SELECT * FROM `service_info`;");
        $STH->execute();
        return $STH->rowCount();
    }
    public function getIndexStat(){
        $result = array();
        $STH  = $this->DBH->prepare("SELECT count(*) FROM `pages` WHERE `status` = 1;");
        $STH->execute();
        $result['query'] = $STH->fetchColumn();
        $STH  = $this->DBH->prepare("SELECT count(*) FROM `pages` WHERE `status` = 2;");
        $STH->execute();
        $result['progress'] = $STH->fetchColumn();
        $STH  = $this->DBH->prepare("SELECT count(*) FROM `pages` WHERE `status` = 3;");
        $STH->execute();
        $result['done'] = $STH->fetchColumn();
        return $result;
    }
    public function regProcess($count){
        $this->DBH->beginTransaction();
        if($this->getProcessCount() < $count){
            $STH = $this->DBH->prepare("INSERT INTO `service_info` VALUES ()");
            $STH->execute();
            $id = $this->DBH->lastInsertId();
            $this->DBH->commit();
            return $id;
        }else{
            $this->DBH->rollBack();
            return null;
        }
    }
    public function unregProcess($id){
       $STH =  $this->DBH->prepare("DELETE FROM `service_info` WHERE `process_id` = :id ");
       $STH->bindParam(':id',$id);
       $STH->execute();
    }
    public function repairSchema(){
        $this->DBH->exec('
        DROP TABLE IF EXISTS `service_info`;
        CREATE  TABLE `service_info` (
          `process_id` INT NOT NULL AUTO_INCREMENT ,
          PRIMARY KEY (`process_id`) )
        ENGINE = InnoDB
        DEFAULT CHARACTER SET = utf8;
        ');
        $pr = IndexerModel::STATUS_INDEXING;
        $q = IndexerModel::STATUS_AWAITING;
        $this->DBH->exec("
          UPDATE `pages` SET `status` = $q WHERE `status` = $pr
        ");
    }

    public function findInContent($string){
        $STH =  $this->DBH->prepare("
            SELECT `hash_url` FROM `search_index`
            WHERE MATCH (`content`) AGAINST (:string) LIMIT 25;
        ");
        $STH->bindParam(':string',$string);
        $STH->setFetchMode(PDO::FETCH_ASSOC);
        $STH->execute();
        $hashs =  $STH->fetchAll();
        $result = array();
        $STH =  $this->DBH->prepare("
            SELECT * FROM `pages`
            WHERE `hash_url`= :hash_url;
        ");
        $STH->bindParam(':hash_url',$hash_url);
        foreach($hashs as $hash){
            $STH->bindParam(':hash_url',$hash['hash_url']);
            $STH->execute();
            $result[] = $STH->fetchObject();
        }
        return $result;
    }
}
