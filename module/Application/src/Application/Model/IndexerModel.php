<?php
/**
 * Created by JetBrains PhpStorm.
 * User: istrelnikov
 * Date: 9/20/13
 * Time: 8:42 PM
 * To change this template use File | Settings | File Templates.
 */

namespace Application\Model;
use Application\Entity\SitePageEntity;

class IndexerModel {
    //----options
    const PROCESS_COUNT = 5;
    //----constants
    const STATUS_AWAITING = 1;
    const STATUS_INDEXING = 2;
    const STATUS_READY = 3;

    private $dao;
    function __construct(){
        $this->dao = new IndexerDAO();
    }

    public function runQueryIndexing(){
        $pr_id = $this->dao->regProcess(IndexerModel::PROCESS_COUNT);
        if($pr_id){
            $page  = $this->dao->getQueryPage();
            if($page){
                $parsed_page = Crawler::getPage($page->getUrl());
                if($parsed_page){
                    foreach($parsed_page['links'] as $link){
                         $this->addToQuery($link);
                    }
                    $this->dao->setPageTitle($page->getHashUrl(),$parsed_page['title']);
                    $this->dao->setPageContent($page->getHashUrl(),$parsed_page['html']);
                    $this->dao->setPagePlainText($page->getHashUrl(),$parsed_page['plain']);
                    $this->dao->setPageStatus($page->getHashUrl(),IndexerModel::STATUS_READY);
                }else{
                    $this->dao->deletePage($page->getHashUrl());
                }
            }
        }
        $this->dao->unregProcess($pr_id);
    }

    function addToQuery($url){
        $url= str_replace('www.','',$url);
        $url = preg_replace('#(?:http(s)?://)?(.+)#', 'http\1://\2', $url);
        return $this->dao->insertNewPage($url);
    }

    function getPageStatus($url){
        $url= str_replace('www.','',$url);
        $url = preg_replace('#(?:http(s)?://)?(.+)#', 'http\1://\2', $url);
        $page = $this->dao->getPageByHashUrl(md5($url));
        if(!is_null($page)){
            $ret = array();
            $ret['status'] = $page->getStatus();
            $ret['date'] = $page->getDate();
            $ret['title'] = $page->getTitle();
            $ret['domain'] = $page->getDomain();
            $ret['hash_url'] = $page->getHashUrl();
            return json_encode($ret);
        }else{
            return null;
        }
    }
    function getDomainInfo($url){
        $url= str_replace('www.','',$url);
        $url = preg_replace('#(?:http(s)?://)?(.+)#', 'http\1://\2', $url);
        $parsed_url = parse_url($url);
        $domain = $parsed_url['host'];
        $result = $this->dao->getDomainInfo($domain);
        if(!is_null($result)){
            return json_encode($result);
        }else{
            return null;
        }
    }
}