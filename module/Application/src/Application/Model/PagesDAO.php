<?php

namespace Application\Model;
use \PDO;
use \PDOException;
use Application\Entity\SitePageEntity;

class PagesDAO {
//params
private $host = 'localhost';
private $dbname = 'site-indexer-phantom';
private $user = 'root';
private $pass = 'root';
private $sqlchar = 'utf8';
//------
const ERROR_DUPLICATE = 1000;
const ERROR_NONE = 200;
private $DBH;

    public function __construct(){
        try {
            # MySQL через PDO_MYSQL
            $this->DBH = new PDO("mysql:host=$this->host;dbname=$this->dbname", $this->user, $this->pass,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
            $this->DBH->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
            $this->DBH->query ( 'SET character_set_connection = '.$this->sqlchar );
            $this->DBH->query ( 'SET character_set_client = '.$this->sqlchar );
            $this->DBH->query ( 'SET character_set_results = '.$this->sqlchar );
        }
        catch(PDOException $e) {
            file_put_contents('PDOErrors.txt', $e->getMessage(), FILE_APPEND);
        }
    }
    public function forceSchemaReinstall(){
        $this->DBH->exec("
            DROP TABLE IF EXISTS `pages`;
            CREATE  TABLE `pages` (
              `hash_url` VARCHAR(100) NOT NULL ,
              `domain` VARCHAR(100) NOT NULL ,
              `title` VARCHAR(100) NOT NULL ,
              `url` VARCHAR(600) NOT NULL ,
              `date` DATETIME NOT NULL ,
              `status` TINYINT NOT NULL ,
              PRIMARY KEY (`hash_url`) ,
              UNIQUE INDEX `hash_url_UNIQUE` (`hash_url` ASC) ,
              INDEX `domain_index` (`domain` ASC) ,
              INDEX `hash_url_index` (`hash_url` ASC) ,
              INDEX `status_index` (`status` ASC) ,
              INDEX `date_status_index` (`status` ASC, `date` ASC) ,
              INDEX `domain_status_index` USING BTREE (`domain` ASC, `status` ASC) )
            ENGINE = InnoDB
            DEFAULT CHARACTER SET = utf8;
        ");
        $this->DBH->exec('
        DROP TABLE IF EXISTS `page_content`;
        CREATE  TABLE `page_content` (
          `content` TEXT NULL ,
          `hash_url` VARCHAR(100) NOT NULL ,
          PRIMARY KEY (`hash_url`) ,
          UNIQUE INDEX `hash_url_UNIQUE` (`hash_url` ASC),
          FULLTEXT (`content`))
        ENGINE = MyISAM
        DEFAULT CHARACTER SET = utf8;
        ');
        $this->DBH->exec('
        DROP TABLE IF EXISTS `attachments`;
        CREATE  TABLE `attachments` (
          `hash_url` VARCHAR(100) NOT NULL ,
          `attachment_type` VARCHAR(10) NOT NULL ,
          `attachment_id` INT NOT NULL AUTO_INCREMENT ,
          `attachment_name` VARCHAR(45) NOT NULL ,
          PRIMARY KEY (`attachment_id`) ,
          INDEX `type_hash_index` (`hash_url` ASC, `attachment_type` ASC) ,
          INDEX `id_index` (`attachment_id` ASC) ,
          UNIQUE INDEX `attachment_id_UNIQUE` (`attachment_id` ASC) )
        ENGINE = InnoDB
        DEFAULT CHARACTER SET = utf8;
        ALTER TABLE `attachments` DROP COLUMN `attachment_id` , ADD COLUMN `attachment_url` VARCHAR(1000) NOT NULL  AFTER `attachment_name` , ADD COLUMN `attachment_hash_url` VARCHAR(100) NOT NULL  AFTER `attachment_url`
        , DROP PRIMARY KEY
        , ADD PRIMARY KEY (`attachment_hash_url`)
        , ADD UNIQUE INDEX `attachment_hash_url_UNIQUE` (`attachment_hash_url` ASC)
        , DROP INDEX `id_index`
        , DROP INDEX `attachment_id_UNIQUE` ;
        ');
        $this->DBH->exec('
        DROP TABLE IF EXISTS `service_info`;
        CREATE  TABLE `service_info` (
          `process_id` INT NOT NULL AUTO_INCREMENT ,
          PRIMARY KEY (`process_id`) )
        ENGINE = InnoDB
        DEFAULT CHARACTER SET = utf8;
        ');
    }

    /**
     * @return array
     */
    public function getQueryPages($status){

            $STH = $this->DBH->prepare("
            SELECT * FROM `pages` WHERE `status` = :st
            ");
            $STH->bindParam(':st',$status);
            $STH->execute();
            $STH->setFetchMode(PDO::FETCH_ASSOC);
            return $STH->fetchAll();
    }
    /**
     * @return array
     */
    public function getQueryAttachments($hash_url){
        $STH = $this->DBH->prepare("
            SELECT * FROM `attachments` WHERE `hash_url` = :hash_url AND `attachment_name` = :name
            ");
        $name = 'unavailable';
        $STH->bindParam(':name',$name);
        $STH->bindParam(':hash_url',$hash_url);
        $STH->execute();
        $STH->setFetchMode(PDO::FETCH_ASSOC);
        return $STH->fetchAll();
    }
    /**
     * @param $page SitePageEntity
     * @return bool|string
     * @throws PDOException
     */
    public function insertPage($page){
        try{
            $STH = $this->DBH->prepare("
            INSERT INTO `pages` (`hash_url`, `domain`,`title`, `url`, `date`, `status`) VALUES (:hash_url , :domain,:title, :url, :date, :status)
            ");
            $STH->bindParam(':hash_url',$page->getHashUrl());
            $STH->bindParam(':url',$page->getUrl());
            $STH->bindParam(':domain',$page->getDomain());
            $STH->bindParam(':title',$page->getTitle());
            $STH->bindParam(':date', $page->getDate());
            $STH->bindParam(':status',$page->getStatus());
            $STH->execute();
            return PageDAO::ERROR_NONE;
        }catch (PDOException $e){
            if($e->getCode() == 23000){
                return PageDAO::ERROR_DUPLICATE;
            }
            file_put_contents('PDOErrors.txt', $e->getMessage(), FILE_APPEND);
            throw new PDOException('Uncatched in PageDao:' .$e->getMessage());
        }
    }
    public function insertPageContent($hash,$content){
        try{
            $STH = $this->DBH->prepare("
            INSERT INTO `page_content` (`hash_url`, `content`) VALUES (:hash_url , :content)
            ");
            $STH->bindParam(':hash_url',$hash);
            $STH->bindParam(':content',$content);
            $STH->execute();
            return PageDAO::ERROR_NONE;
        }catch (PDOException $e){
            if($e->getCode() == 23000){
                $STH = $this->DBH->prepare("
                UPDATE `page_content` SET `content` = :content WHERE `hash_url` = :hash_url
                ");
                $STH->bindParam(':hash_url',$hash);
                $STH->bindParam(':content',$content);
                $STH->execute();
                return PageDAO::ERROR_NONE;
            }
            file_put_contents('PDOErrors.txt', $e->getMessage(), FILE_APPEND);
            throw new PDOException('Uncatched in PageDao:' .$e->getMessage());
        }
    }
    public function setPageStatus($hash,$status){
        $STH = $this->DBH->prepare("
        UPDATE `pages` SET `status` = :status WHERE `hash_url` = :hash_url
        ");
        $STH->bindParam(':hash_url',$hash);
        $STH->bindParam(':status',$status);
        $STH->execute();
        return PageDAO::ERROR_NONE;
    }
    public function setPageTitle($hash,$title){
        $STH = $this->DBH->prepare("
        UPDATE `pages` SET `title` = :title WHERE `hash_url` = :hash_url
        ");
        $STH->bindParam(':hash_url',$hash);
        $STH->bindParam(':title',$title);
        $STH->execute();
        return PageDAO::ERROR_NONE;
    }
    public function setAttachmentName($hash,$name){
        $STH = $this->DBH->prepare("
        UPDATE `attachments` SET `attachment_name` = :name WHERE `attachment_hash_url` = :hash_url
        ");
        $STH->bindParam(':hash_url',$hash);
        $STH->bindParam(':name',$name);
        $STH->execute();
        return PageDAO::ERROR_NONE;
    }

    public function insertAttachment($hash,$type,$at_url){
        try{
            $STH = $this->DBH->prepare("
            INSERT INTO `attachments` (`hash_url`, `attachment_type`, `attachment_name`,`attachment_url`,`attachment_hash_url`) VALUES (:hash_url , :type, :name,:at_url,:at_hash_url)
            ");
            $STH->bindParam(':hash_url',$hash);
            $STH->bindParam(':type',$type);
            $name = 'unavailable';
            $STH->bindParam(':name',$name);
            $STH->bindParam(':at_url',$at_url);
            $STH->bindParam(':at_hash_url',md5($at_url));
            $STH->execute();
            return PageDAO::ERROR_NONE;
        }catch (PDOException $e){
            if($e->getCode() == 23000){
                return PageDAO::ERROR_DUPLICATE;
            }
            file_put_contents('PDOErrors.txt', $e->getMessage(), FILE_APPEND);
            throw new PDOException('Uncatched in PageDao:' .$e->getMessage());
        }
    }
    public function getAttachment($at_hash){
        try{
            $STH = $this->DBH->prepare("
                SELECT * FROM attachments WHERE attachment_hash_url = :hash_url
            ");
            $STH->bindParam(':hash_url',$at_hash);
            $STH->execute();
            $STH->setFetchMode(PDO::FETCH_ASSOC);
            $attachs =$STH->fetch();
            return $attachs;
        }catch (PDOException $e){
            file_put_contents('PDOErrors.txt', $e->getMessage(), FILE_APPEND);
            throw new PDOException('Uncatched in PageDao:' .$e->getMessage());
        }
    }
    public function deleteAttachment($hash){
        $STH =  $this->DBH->prepare("DELETE FROM `attachments` WHERE `attachment_hash_url` = :hash_url ");
        $STH->bindParam(':hash_url',$hash);
        $STH->execute();
    }
    public function deletePage($hash){
        $STH =  $this->DBH->prepare("DELETE FROM `pages` WHERE `hash_url` = :hash_url ");
        $STH->bindParam(':hash_url',$hash);
        $STH->execute();
    }
    public function getPageByHashUrl($hash){
        try{
            $STH = $this->DBH->prepare("
                SELECT * FROM pages WHERE hash_url = :hash_url
            ");
            $STH->bindParam(':hash_url',$hash);
            $STH->execute();
            $STH->setFetchMode(PDO::FETCH_ASSOC);
            $page =$STH->fetch();
            return $page;
        }catch (PDOException $e){
            file_put_contents('PDOErrors.txt', $e->getMessage(), FILE_APPEND);
            throw new PDOException('Uncatched in PageDao:' .$e->getMessage());
        }
    }
    public function getAttachmentsByHashUrl($hash){
        try{
            $STH = $this->DBH->prepare("
                SELECT * FROM attachments WHERE hash_url = :hash_url
            ");
            $STH->bindParam(':hash_url',$hash);
            $STH->execute();
            $STH->setFetchMode(PDO::FETCH_ASSOC);
            $attachs =$STH->fetchAll();
            return $attachs;
        }catch (PDOException $e){
            file_put_contents('PDOErrors.txt', $e->getMessage(), FILE_APPEND);
            throw new PDOException('Uncatched in PageDao:' .$e->getMessage());
        }
    }

    public function getProcessCount(){
        $STH  = $this->DBH->prepare("SELECT * FROM `service_info`;
                          ");
        $STH->execute();
        return $STH->rowCount();
    }
    public function regProcess(){
        $STH = $this->DBH->prepare("INSERT INTO `service_info` VALUES ()");
        $STH->execute();
        return $this->DBH->lastInsertId();
    }
    public function unregProcess($id){
       $STH =  $this->DBH->prepare("DELETE FROM `service_info` WHERE `process_id` = :id ");
       $STH->bindParam(':id',$id);
       $STH->execute();
    }
    public function findInContent($string){
        $STH =  $this->DBH->prepare("
            SELECT `hash_url` FROM `page_content`
            WHERE MATCH (`content`) AGAINST (:string) LIMIT 25;
        ");
        $STH->bindParam(':string',$string);
        $STH->setFetchMode(PDO::FETCH_ASSOC);
        $STH->execute();
        $hashs =  $STH->fetchAll();
        $result = array();
        $STH =  $this->DBH->prepare("
            SELECT `hash_url`,`url`,`domain`,`date`,`title` FROM `pages`
            WHERE `hash_url`= :hash_url;
        ");
        $STH->bindParam(':hash_url',$hash_url);
        foreach($hashs as $hash){
            $STH->bindParam(':hash_url',$hash['hash_url']);
            $STH->execute();
            $result[] = $STH->fetchObject();

        }
        return $result;
    }
}
