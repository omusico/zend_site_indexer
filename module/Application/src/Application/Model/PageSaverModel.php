<?php
/**
 * Created by JetBrains PhpStorm.
 * User: istrelnikov
 * Date: 9/20/13
 * Time: 8:42 PM
 * To change this template use File | Settings | File Templates.
 */

namespace Application\Model;
use Application\Entity\SitePageEntity;

class PageSaverModel {
    private $dao;
    private $p_model;
    function __construct(){
        $this->dao = new PageDAO();
        $this->p_model = new SitePageModel();
    }
    public function runQueryIndexing(){
        if($this->dao->getProcessCount() < 1){
            $pr_id = $this->dao->regProcess();
            $pages = $this->dao->getQueryPages(SitePageModel::STATUS_INDEXING);
            /**
             * @var $page SitePageEntity
             */
            foreach($pages as $page){
                $parsed_page = Crawler::getPage($page['url']);

                $path_html = Crawler::uri_to_local($page['url'],'html',$page['url']);
                if(!is_dir('public'.$path_html['path'])){
                    mkdir('public'.$path_html['path'], 0755, true);
                }
                $parsed_page['html'] = $this->parseCss($parsed_page['html'],$page['url'],$page['url'],true);
                file_put_contents('public'.$path_html['path'].'/index.html',$parsed_page['html']);

                if($parsed_page){
                    foreach($parsed_page['links'] as $link){
                        //TODO: options - deep and domain only;
                        $parsed_link = parse_url($link);
                        if($page['domain'] == $parsed_link['host']){
                            $this->p_model->addToQuery($link);
                        }
                    }
                    foreach($parsed_page['css'] as $css_url){
                        $this->dao->insertAttachment($page['hash_url'],'css',$css_url);//Suppress duplicate
                    }
                    foreach($parsed_page['scripts'] as $js_url){
                        $this->dao->insertAttachment($page['hash_url'],'script',$js_url);//Suppress duplicate
                    }
                    foreach($parsed_page['images'] as $img_url){
                        $this->dao->insertAttachment($page['hash_url'],'image',$img_url);//Suppress duplicate
                    }
                    $this->dao->setPageTitle($page['hash_url'],$parsed_page['title']);
                    $this->dao->insertPageContent($page['hash_url'],$parsed_page['content']);
                    $this->dao->setPageStatus($page['hash_url'],SitePageModel::STATUS_DOWNLOADING);
                    $this->runQueryDownloading();
                }else{
                        $this->dao->deletePage($page['hash_url']);
                }
            }
            $this->dao->unregProcess($pr_id);
        }
    }
    public function runQueryDownloading(){
        $pages = $this->dao->getQueryPages(SitePageModel::STATUS_DOWNLOADING);
        foreach($pages as $page){
            $attachments = $this->dao->getQueryAttachments($page['hash_url']);

            foreach($attachments as $attach){
                $this->downloadAttach($attach,$page['url']);
            }

            $this->dao->setPageStatus($page['hash_url'],SitePageModel::STATUS_READY);
        }
    }
    private function parseCss($content,$link,$at_url,$html=false){
        $pattern = '/@import[ \("\']*([^;")\]\']+)[;\)"\']*/si';
        if($html){
            $pattern = '/<.*style.*@import[ \("\']*([^;{}<>")\]\']+)[;\)"\']*.*<\/style>/si';
        }
        preg_match_all($pattern, $content, $innerCSS);
        if(isset($innerCSS[1])){
            foreach ($innerCSS[1] as $in_css) {
                $uri = Crawler::prepareUri($in_css);
                if(!$uri){
                    continue;
                }
                require_once("url_to_absolute.php");
                $uri = url_to_absolute($at_url,$uri);
                $local = Crawler::uri_to_local($uri,'css',$link);
                $this->dao->insertAttachment(md5($link),'css',$uri);//Suppress duplicate
                $attach = $this->dao->getAttachment(md5($uri));
                $this->downloadAttach($attach,$link);
                $content = str_replace($in_css,$local['path'].'/'.$local['name'], $content,intval(1));
            }
        }
        $pattern = '/background[^<>;]*url[ \("\']*([^;")\]\']+)[;\)"\']*/si';
        preg_match_all($pattern, $content, $innerCSS);
        if(isset($innerCSS[1])){
            foreach ($innerCSS[1] as $in_css) {
                file_put_contents('in_css_background.txt',$in_css,FILE_APPEND);
                $uri = Crawler::prepareUri($in_css);
                if(substr($uri, 0, 5) === 'data:'){
                    continue;
                }
                require_once("url_to_absolute.php");
                $uri = url_to_absolute($at_url,$uri);
                $local = Crawler::uri_to_local($uri,'image',$link);
                $this->dao->insertAttachment(md5($link),'image',$uri);//Suppress duplicate
                $attach = $this->dao->getAttachment(md5($uri));
                $this->downloadAttach($attach,$link);
                $content = str_replace($in_css,$local['path'].'/'.$local['name'], $content,intval(1));
            }
        }
        return $content;
    }

    private function downloadAttach($attach,$url){
        $content = $this->get_file_contents($attach['attachment_url']);
        $path = Crawler::uri_to_local($attach['attachment_url'],$attach['attachment_type'],$url);
        if(!is_dir('public'.$path['path'])){
            mkdir('public'.$path['path'], 0777, true);
        }
        if(!$content || strlen($path['name']) <2 || strlen($path['name']) > 200){
            $this->dao->deleteAttachment($attach['attachment_hash_url']);
            return;
        }
        if($attach['attachment_type']=='css'){
            $content = $this->parseCss($content,$url,$attach['attachment_url']);
        }
        file_put_contents('public'.$path['path'].'/'.$path['name'], $content);
        $this->dao->setAttachmentName($attach['attachment_hash_url'],$path['name']);
    }
    private function get_file_contents($url)
    {
        $ctx = stream_context_create(array(
                'http' => array(
                    'timeout' => 20
                )
            )
        );
        $Tries = 0;
        do{
            if ($Tries > 0) sleep(1); # Wait for a sec before retrieving again
            $contents = @file_get_contents($url,0,$ctx);
            $Tries++;
            if ($contents == "") $contents = FALSE;
            if($Tries == 5){
                file_put_contents('failed.txt',"\r\n".$url,FILE_APPEND);
            }
        } while ($Tries <= 5 && $contents === FALSE);
        return $contents;
    }
}