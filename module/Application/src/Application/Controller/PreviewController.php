<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Application\Model\IndexerDAO;
use Application\Model\IndexerModel;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Http\Response;

class PreviewController extends AbstractActionController{
    public function indexAction(){
        $hash = $this->params()->fromQuery('url');
        $dao = new IndexerDAO();
        $page = $dao->getPageByHashUrl($hash);

        $response='<h1 style="text-align: center">This page have status: ';
        switch($page->getStatus()){
            case IndexerModel::STATUS_AWAITING;
                $response.=' in index query</h1>';
                break;
            case IndexerModel::STATUS_INDEXING:
                $response.=' indexing in process</h1>';
                break;
            case IndexerModel::STATUS_READY:
                $response = '<div style="position:absolute;left: 15px;top:15px;z-index:1000;opacity: 0.3;filter: alpha(opacity=30);fo"><h2>This is saved copy by tt-indexer service. Original content may be changed(Last indexed:'.$page->getDate().')</h2></div>'.
                   $page->getHTML();
                break;
            default:
                $response.=' not in database</h1>';
        }
        return $this->getResponse()->setContent($response);
    }
}
