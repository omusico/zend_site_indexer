<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Application\Model\Crawler;
use Application\Model\IndexerDAO;
use Application\Model\IndexerModel;
use Application\Model\PageDAO;
use Application\Model\SitePageModel;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Http\Response;

class IndexerController extends AbstractActionController
{
    public function indexAction(){
        return new ViewModel();
    }

    public function toqueryAction(){
      $data = json_decode($this->getRequest()->getContent());
      $indexer= new IndexerModel();
      $msg= $indexer->addToQuery($data->url);
      return  $this->getResponse()->setContent($msg);
    }

    public function gethashAction(){
        $data = json_decode($this->getRequest()->getContent());
        $url= str_replace('www.','',$data->url);
        $url = preg_replace('#(?:http(s)?://)?(.+)#', 'http\1://\2', $url);
        return $this->getResponse()->setContent(md5($url));
    }
    public function checkAction(){
        $data = json_decode($this->getRequest()->getContent());
        $indexer= new IndexerModel();
        $msg = $indexer->getPageStatus($data->url);
        return  $this->getResponse()->setContent($msg);
    }
    public function domaininfoAction(){
        $data = json_decode($this->getRequest()->getContent());
        $indexer= new IndexerModel();
        $msg = $indexer->getDomainInfo($data->url);
        return  $this->getResponse()->setContent($msg);
    }
    public function searchAction(){
        $data = json_decode($this->getRequest()->getContent());
        $dao = new IndexerDAO();
        $results = $dao->findInContent($data->text);
        return  $this->getResponse()->setContent(json_encode($results));
    }
    public function statAction(){
        $dao = new IndexerDAO();
        $results = $dao->getIndexStat();
        return  $this->getResponse()->setContent(json_encode($results));
    }
}