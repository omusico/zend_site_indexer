var casper = require('casper').create();
var address = casper.cli.args[0];
var result = {'html':'','plain':'','title':''};
casper.start(address, function() {

});
casper.run(function() {
    result.html = this.getHTML();
    result.plain = this.page.plainText;
    result.title = this.evaluate(function(){
        return document.title;
    });
    this.echo(JSON.stringify(result));
    this.exit();
});