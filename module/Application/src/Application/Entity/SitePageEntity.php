<?php


namespace Application\Entity;

/**
 * Class SitePageEntity
 * @package Application\Entity
 */
class SitePageEntity{
    private $hashUrl;
    private $url;
    private $domain;
    private $title;
    private $status;
    private $date;
    private $html;


    function __construct(){
        if(func_get_arg(0)){
            $assoc_array = func_get_arg(0);
            $this->date = $assoc_array['date'];
            $this->domain = $assoc_array['domain'];
            $this->hashUrl = $assoc_array['hash_url'];
            $this->html = $assoc_array['html'];
            $this->status = $assoc_array['status'];
            $this->title = $assoc_array['title'];
            $this->url = $assoc_array['url'];
        }
    }
    /**
     * Set hashUrl
     *
     * @param string $hashUrl
     * @return SitePageEntity
     */
    public function setHashUrl($hashUrl){
        $this->hashUrl = $hashUrl;
        return $this;
    }

    /**
     * Get hashUrl
     *
     * @return string 
     */
    public function getHashUrl(){
        return $this->hashUrl;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return SitePageEntity
     */
    public function setUrl($url){
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl(){
        return $this->url;
    }

    /**
     * Set baseUrl
     *
     * @param string $domain
     * @return SitePageEntity
     */
    public function setDomain($domain){
        $this->domain = $domain;
        return $this;
    }

    /**
     * Get baseUrl
     *
     * @return string 
     */
    public function getDomain(){
        return $this->domain;
    }
    /**
     * Set title
     *
     * @param string $title
     * @return SitePageEntity
     */
    public function setTitle($title){
        $this->title = $title;
        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle(){
        return $this->title;
    }
    /**
     * Set status
     *
     * @param int $status
     * @return SitePageEntity
     */
    public function setStatus($status){
        $this->status = $status;
        return $this;
    }

    /**
     * Get status
     *
     * @return int
     */
    public function getStatus(){
        return $this->status;
    }
    /**
     * Set status
     *
     * @param String $date
     * @return SitePageEntity
     */
    public function setDate($date){
        $this->date = $date;
        return $this;
    }
    /**
     * Get status
     *
     * @return \DateTime
     */
    public function getDate(){
        return $this->date;
    }
    /**
     * Set html
     *
     * @param string $html
     * @return SitePageEntity
     */
    public function setHTML($html){
        $this->html = $html;
        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getHTML(){
        return $this->html;
    }
}
